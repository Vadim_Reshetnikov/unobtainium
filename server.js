var app = require('express')()
var http = require('http').Server(app);
var io = require('socket.io')(http, { origins: '*:*'}) 

const PORT=8080; 
var counter=0



function getAvaliableProducts() {
    return [
        {
            name: 'Condensed veldspar',
            price: '219.33',
            inStock: 5
        },
        {
            name: 'Cordite',
            price: '55.12',
            inStock: 2
        },
        {
            name: 'Unobtainium',
            price: '10000.00',
            inStock: 1
        },
        {
            name: 'Mineral A',
            price: '10',
            inStock: 1
        },
        {
            name: 'Mineral B',
            price: '10',
            inStock: 1
        },
        {
            name: 'Mineral C',
            price: '10',
            inStock: 1
        },
        {
            name: 'Mineral D',
            price: '10',
            inStock: 1
        },
        {
            name: 'Mineral E',
            price: '10',
            inStock: 1
        },
    ]
}

var products = getAvaliableProducts()

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html')
})

http.listen(PORT, function(){
    console.log("Server listening on: http://localhost:%s", PORT);
});

io.on('connection', (socket) => {
    console.log(`POS ${++counter} connected to a server`)
    socket.on('disconnect', () => {
        console.log(`POS ${counter} disconnected`)
    })
    // Посылаем список на клиент при подключении
    socket.emit('update products', products)
    // Получили заказ, обработали, отослали всем клиентам обновление
    socket.on('process order', (order) => {
        const pArray = order.products
            pArray.map((val) => {
                const index = match(val, products)
                products[index].inStock -= val.quantity
            })
        socket.broadcast.emit('update products', products)
    })
})